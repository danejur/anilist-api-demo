﻿using AniListApiDemo.Models;
using RestSharp;
using System;
using System.Configuration;

namespace AniListApiDemo
{
    class Program
    {
        private static readonly string ApiBasePath = "https://anilist.co/api/";
        private static string ClientId;
        private static string ClientSecret;
        private static string RedirectUri;
        private static string AccessToken;

        static void Main(string[] args)
        {
            //first we need to make sure we have a client id, client secret, and redirect uri
            ClientId = ConfigurationManager.AppSettings["ClientId"];
            ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            RedirectUri = ConfigurationManager.AppSettings["RedirectUri"];

            if (string.IsNullOrWhiteSpace(ClientId) || string.IsNullOrWhiteSpace(ClientSecret) || string.IsNullOrWhiteSpace(RedirectUri))
            {
                Console.WriteLine("Please make sure you have a Client ID, Client Secret, and Redirect URI in your AppConfig!");
                return;
            }

            /* at this point, assuming we have a valid Client ID, Client Secret, and Redirect URI,
             * we need to direct the user to the AniList authorization page with the correct client id
             * and redirect uri in the URL itself. We'll output that to the console now. */

            var authUrl = $"http://anilist.co/api/auth/authorize?grant_type=authorization_code&client_id={ClientId}&response_type=code&redirect_uri={RedirectUri}";
            Console.WriteLine("Please go to the following URL in your web browser and login to AniList to authorize the application.\nOnce you have been redirected after authorization, please copy the \"code\" that you see in the URL.\nFor example, in the following URL (https://anilist.co?code=12345), the code would be \"12345\" (without quotes).");
            Console.WriteLine($"\n{authUrl}");
            Console.WriteLine("\nEnter code:");

            var code = Console.ReadLine();

            /* now that we have a code, lets do our first API call. We are going to be
             * authorizing for the first time against the AniList API, so for this call, we
             * will be passing the "code" we just got in the last step. The method below called 
             * AuthorizeWithAuthCode will take in a "code" parameter and return an AuthorizationResponse
             * object that contains the data we need from our first auth. */

            var initialAuth = AuthorizeWithAuthCode(code);

            /* once we have our initial authentication out of the way, we will have an important
             * piece of data that we need to record. This is our "refresh token". This allows us to 
             * re-authenticate in the future without having to go to AniList and approving again. Record
             * this code in the app.config, or some other non-volatile storage. Keep in mind that this code
             * is user specific and an authentication token, so take care that it is not readable by other parties! */

            Console.WriteLine($"\nOur current access token is {initialAuth.Access_Token}.\nAdditionally, we got a refresh token, and it is {initialAuth.Refresh_Token}.\nPress any key to continue.");
            Console.ReadKey();

            /* at this point, we should try using the refresh token to re-authenticate. If everything
             * has gone correctly up to this point, we should be able to re-auth without any hassle. 
             * Note: our new access token will be different, but our refresh token will remain the same. */

            var secondAuth = AuthorizeWithRefreshToken(initialAuth.Refresh_Token);

            Console.WriteLine($"\nOur current (new) access token is {secondAuth.Access_Token}.\nPress any key to continue.");
            Console.ReadKey();

            /* now that we have a refresh token to re-authenticate with in the future, lets try
             * getting some AniList data. We'll start with a simple request for an Anime object. */

            //first lets store our access token so we can call it from the other methods (this isn't best practice, but works for our demo)
            AccessToken = secondAuth.Access_Token;

            var animeId = 1;
            var anime = GetAnimeById(animeId);

            Console.WriteLine($"\nWe fetched an anime with the ID of {animeId}, and the romaji title is {anime.Title_Romaji}.\nThe airing status is {anime.Airing_Status} and the average score is {anime.Average_Score}.\nPress any key to continue.");
            Console.ReadKey();

            /* now that we got our anime, lets try getting user data. 
             * First, we'll enter the username of the user we want to data on. */

            Console.WriteLine("\nEnter a username to fetch data for:");
            var userName = Console.ReadLine();

            var user = GetUserByName(userName);

            Console.WriteLine($"\nWe got a user with the username of {userName}. His/her total anime time is {user.Anime_Time}.\nPress any key to end.");
        }

        static AuthorizationResponse AuthorizeWithAuthCode(string authCode)
        {
            var client = new RestClient(ApiBasePath);

            var authReq = new RestRequest("auth/access_token", Method.POST);

            authReq.AddParameter("client_id", ClientId);
            authReq.AddParameter("client_secret", ClientSecret);
            authReq.AddParameter("grant_type", "authorization_code");
            authReq.AddParameter("redirect_uri", RedirectUri);
            authReq.AddParameter("code", authCode);

            var authResp = client.Execute<AuthorizationResponse>(authReq);

            //if our response is a 200, it worked and we should have some data
            if (authResp.StatusCode == System.Net.HttpStatusCode.OK)
                return authResp.Data;

            //if it isn't, our request may have been faulty. check the client id, client secret, and redirect uri to make sure they are identical with what is listed on the devloper section at AniList.
            else
                throw new Exception("There was a problem authenticating with the supplied code. Are all the parameters correct?");
        }

        static AuthorizationResponse AuthorizeWithRefreshToken(string refreshToken)
        {
            var client = new RestClient(ApiBasePath);

            var authReq = new RestRequest("auth/access_token", Method.POST);

            authReq.AddParameter("client_id", ClientId);
            authReq.AddParameter("client_secret", ClientSecret);
            authReq.AddParameter("grant_type", "refresh_token");
            authReq.AddParameter("redirect_uri", RedirectUri);
            authReq.AddParameter("refresh_token", refreshToken);

            var authResp = client.Execute<AuthorizationResponse>(authReq);

            //if our response is a 200, our re-auth should have worked
            if (authResp.StatusCode == System.Net.HttpStatusCode.OK)
                return authResp.Data;

            //if it isn't, our first auth may have not worked correctly. be sure to check that the response had data in it, and that the refresh token and access token had actual values.
            else
                throw new Exception("There was a problem authenticating with the supplied refresh token. Did the first auth actually work?");
        }

        static Anime GetAnimeById(int id)
        {
            var client = new RestClient(ApiBasePath);

            var animeReq = new RestRequest("anime/{id}", Method.GET);
            animeReq.AddUrlSegment("id", id.ToString());

            //since we are trying to access AniList data, we need to supply the access_token we got from authenticating
            //note that these do have an expiration time, so re-authentication is necessary every so often.
            animeReq.AddParameter("access_token", AccessToken);

            var animeResp = client.Execute<Anime>(animeReq);

            //once again, if our status code was 200, we should have an Anime waiting for us
            if (animeResp.StatusCode == System.Net.HttpStatusCode.OK)
                return animeResp.Data;

            //if it isn't, there are a couple possibilities:
            //404 - the ID we supplied doesn't map to an anime title on AniList
            //401 - we aren't currently authenticated
            //400 - some other problem with our request
            //500 - the server threw an exception, but it could have been caused by a problem with our request
            else
                throw new Exception($"There was a problem getting the anime. The status code returned was {(int)animeResp.StatusCode}.");
        }

        static User GetUserByName(string name)
        {
            var client = new RestClient(ApiBasePath);

            var userReq = new RestRequest("user/{name}", Method.GET);
            userReq.AddUrlSegment("name", name);

            //like before, we'll need to supply our access token again
            userReq.AddParameter("access_token", AccessToken);

            var userResp = client.Execute<User>(userReq);

            //and again, if our status code was 200, we should have a user
            if (userResp.StatusCode == System.Net.HttpStatusCode.OK)
                return userResp.Data;

            //like before, if we didn't get a 200, it could be for a couple different reasons.
            else
                throw new Exception($"There was a problem getting the user. The status code returned was {(int)userResp.StatusCode}.");
        }
    }
}
