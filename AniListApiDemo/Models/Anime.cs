﻿using System;

namespace AniListApiDemo.Models
{
    public class Anime
    {
        public int Id { get; set; }
        public string Title_Romaji { get; set; }
        public string Type { get; set; }
        public string Image_Url_Med { get; set; }
        public string Image_Url_Sml { get; set; }
        public DateTime? Start_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public string Classification { get; set; }
        public string Hashtag { get; set; }
        public string Source { get; set; }
        public string Title_Japanese { get; set; }
        public string Title_English { get; set; }
        public string Description { get; set; }
        public string Image_Url_Lge { get; set; }
        public string Image_Url_Banner { get; set; }
        public int? Duration { get; set; }
        public string Airing_Status { get; set; }
        public double Average_Score { get; set; }
        public int Total_Episodes { get; set; }
        public string Youtube_Id { get; set; }
        public bool Adult { get; set; }
        public string Relation_Type { get; set; }
        public string Role { get; set; }
        public int User_Rating { get; set; }
        public bool Favourite { get; set; }
        public DateTime Last_Updated { get; set; }
        public int Popularity { get; set; }
        public DateTime? CacheDate { get; set; }
    }
}

