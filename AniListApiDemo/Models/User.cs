﻿namespace AniListApiDemo.Models
{
	public class User
	{
		public int Id { get; set; }
		public string Display_Name { get; set; }
		public int Anime_Time { get; set; }
		public int Manga_Chapters { get; set; }
		public string About { get; set; }
		public int List_Order { get; set; }
		public bool Adult_Content { get; set; }
		public string Forum_Homepage { get; set; }
		public bool Following { get; set; }
		public string Image_Url_Lge { get; set; }
		public string Image_Url_Med { get; set; }
		public string Image_Url_Banner { get; set; }
		public string Title_Language { get; set; }
		public int Score_Type { get; set; }
		public bool Advanced_Rating { get; set; }
		public int Notifications { get; set; }
	}
}

