using System;

namespace AniListApiDemo.Models
{
    public class AuthorizationResponse
    {
        public string Access_Token { get; set; }
        public string Token_Type { get; set; }
        public int Expires { get; set; }
        public int Expires_In { get; set; }
        public string Refresh_Token { get; set; }
        public DateTime ExpiresDateTime { get { return (Expires != 0) ? new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(Expires) : DateTime.MinValue; } }
    }
}